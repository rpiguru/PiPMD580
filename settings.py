
PMD580_VID = 0x067B
PMD580_PID = 0x2303

INIT_SCREEN = 'home_screen'

LOG_FILE = 'event.log'

SCREEN_OFF_LOGGED_IN = 90 * 60
SCREEN_OFF_LOGGED_OUT = 15 * 60

try:
    from local_settings import *
except ImportError:
    pass
