from kivy.factory import Factory

from widgets.button import CircularButton, ImageButton
from widgets.label import ScrollableLabel
from widgets.separator import Separator


Factory.register('Separator', cls=Separator)
Factory.register('CircularButton', cls=CircularButton)
Factory.register('ImageButton', cls=ImageButton)
Factory.register('ScrollableLabel', cls=ScrollableLabel)
