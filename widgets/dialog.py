import random
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty
from kivy.uix.modalview import ModalView
from kivy.uix.popup import Popup

from utils.common import validate_pwd
from widgets.button import CircularButton

Builder.load_file('widgets/kv/dialog.kv')


class PiPMDModalView(ModalView):
    pass


class PasswordDialog(PiPMDModalView):

    pwd = StringProperty('')
    is_pwd = BooleanProperty(True)
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        super(PasswordDialog, self).__init__(**kwargs)
        self.register_event_type('on_success')

        for i in [1, 2, 3, 4, 5, 6, 7, 8, 9]:
            self._add_numeric_btn(i)
        btn = CircularButton(text='CLR', font_size=20)
        btn.bind(on_release=self.on_btn_clear)
        self.ids.box.add_widget(btn)
        self._add_numeric_btn(0)
        btn = CircularButton(text='ENTER', font_size=20)
        btn.bind(on_release=self.on_btn_enter)
        self.ids.box.add_widget(btn)
        self.pos_hint = {'center_x': random.randint(25, 75) / 100.}

    def _add_numeric_btn(self, num):
        _btn = CircularButton(text=str(num))
        _btn.bind(on_release=self.on_numeric_btn_pressed)
        self.ids.box.add_widget(_btn)

    def on_success(self, *args):
        pass

    def on_numeric_btn_pressed(self, *args):
        if len(self.pwd) < 10:
            self.pwd += args[0].text

    def on_btn_enter(self, *args):
        if self.is_error:
            return
        if self.pwd:
            if self.is_pwd:
                user = validate_pwd(pwd=self.pwd)
                if user:
                    self.dispatch('on_success', user)
                else:
                    self.is_error = True
            else:
                self.dispatch('on_success')
        else:
            self.is_error = True

    def on_btn_clear(self, *args):
        self.pwd = ''
        self.is_error = False


class YesNoDialog(Popup):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_btn_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class BlackPopup(ModalView):

    def on_touch_down(self, touch):
        self.dismiss()
        return False


class InfoPopup(Popup):
    pass
