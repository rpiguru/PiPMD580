import os
from kivy.lang import Builder
from kivy.properties import ListProperty
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image
from kivy.uix.label import Label
from kivy.vector import Vector


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'button.kv'))


class CircularButton(ButtonBehavior, Label):

    background_color = ListProperty([.8, .8, .8, 1])

    def collide_point(self, x, y):
        return Vector(x, y).distance(self.center) <= self.width / 2


class ImageButton(ButtonBehavior, Image):
    pass
