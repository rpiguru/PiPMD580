import conf.config_before
import gc
import time

from kivy.app import App
import conf.config_kivy
import widgets.factory_reg

from kivy.uix.screenmanager import FadeTransition
from kivy.properties import BooleanProperty, DictProperty, ObjectProperty
from kivy.clock import Clock
from kivy.uix.label import Label
from screens.screen_manager import sm, screens
from settings import INIT_SCREEN
from utils.common import get_free_gpu_size, is_rpi, add_log
from utils import logger
from utils.pmd580 import PMD580
from widgets.dialog import InfoPopup


class PiPMD580App(App):

    locked = BooleanProperty(True)
    current_screen = None
    pmd = None
    cur_user = DictProperty()
    info_popup = ObjectProperty(None, allownone=True)

    def build(self):
        self.pmd = PMD580(callback=self.on_pmd_msg)
        self.pmd.start()
        self.switch_screen(INIT_SCREEN)
        if not is_rpi():
            Clock.schedule_once(lambda dt: self.on_pmd_msg({'type': 'card', 'value': 'Card In'}), 2)
            Clock.schedule_once(lambda dt: self.on_pmd_msg({'type': 'card', 'value': 'Card Error'}), 3)
        return sm

    def switch_screen(self, screen_name, duration=.3):
        s_time = time.time()
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            sm.transition = FadeTransition(duration=duration)
            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            logger.info('===== Switched to {} screen {}, Elapsed: {} ====='.format(
                screen_name, msg, time.time() - s_time))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def on_pmd_msg(self, buf):
        logger.info('----------')
        logger.info(buf)
        add_log("Message from PMW580 - {}".format(buf))
        logger.info('----------')
        if buf['type'] is not None and buf['type'] != 'state':
            if self.info_popup is None:
                self.info_popup = InfoPopup(title=buf['type'].capitalize(), title_size=25,
                                            content=Label(text=buf['value'], font_size=30))
                self.info_popup.bind(on_dismiss=self.on_info_popup_dismissed)
                self.info_popup.open()
            else:
                self.info_popup.title = buf['type'].capitalize()
                self.info_popup.content.text = buf['value']

    def on_info_popup_dismissed(self, *args):
        self.info_popup = None

    def on_stop(self):
        self.pmd.close()
        super(PiPMD580App, self).on_stop()


if __name__ == '__main__':

    app = PiPMD580App()

    app.run()
