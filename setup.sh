#!/usr/bin/env bash

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

echo "========== Setup PiPMD580 =========="
sudo apt update

echo "Install Kivy now..."
sudo apt install -y python3 python3-dev python3-pip
sudo apt install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev
sudo apt install -y libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-bad
sudo apt install -y gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-omx
sudo apt install -y gstreamer1.0-alsa libmtdev-dev xclip

sudo pip3 install -U pip
sudo pip3 install -r requirements.txt
sudo pip3 install Cython==0.25.2

cd /tmp
git clone https://github.com/kivy/kivy.git
cd kivy
git checkout 678a055
sudo pip3 install .
#sudo pip3 install git+https://github.com/kivy/kivy.git@master

echo "Configuring the 7inch touchscreen from Waveshare..."
echo "max_usb_current=1" | sudo tee -a /boot/config.txt
echo "hdmi_group=2" | sudo tee -a /boot/config.txt
echo "hdmi_mode=87" | sudo tee -a /boot/config.txt
echo "hdmi_cvt 1024 600 60 6 0 0 0" | sudo tee -a /boot/config.txt
echo "hdmi_drive=1" | sudo tee -a /boot/config.txt

# Increase GPU memory size
echo "gpu_mem=512" | sudo tee -a /boot/config.txt

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0 plymouth.ignore-serial-consoles plymouth.enable=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

# Disable the blinking cursor
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

# Disable some services to reduce booting time
sudo systemctl disable hciuart
sudo mkdir /etc/systemd/system/networking.service.d
sudo touch /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "[Service]" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
echo "TimeoutStartSec=1" | sudo tee -a /etc/systemd/system/networking.service.d/reduce-timeout.conf
sudo rm /etc/systemd/system/dhcpcd.service.d/wait.conf

# Enable auto start
sudo apt-get install screen

sudo sed -i -- "s/^exit 0/screen -mS pmd -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S pmd -X stuff \"cd \/home\/pi\/PiPMD580\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S pmd -X stuff \"sleep 3 \&\& python3 main.py\\\\r\"\\nexit 0/g" /etc/rc.local

# Install custom splash video
cd ${cur_dir}/scripts
sudo bash install_custom_splash_screen.sh
