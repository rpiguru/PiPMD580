# PiPMD580

Custom Control Interface for a Marantz PMD-580 Digital Recorder using Raspberry Pi

## Components

- Raspberry Pi Model 3B+

- 7" HDMI display from Waveshare
    
    https://www.waveshare.com/product/modules/oleds-lcds/raspberry-pi-lcd/7inch-hdmi-lcd-c.htm

- USB-RS232 adaptor
    
    https://www.startech.com/ca/Cards-Adapters/Serial-Cards-Adapters/USB-to-RS232-Serial-Adapter-Cable~ICUSB232V2


## Installation

- Clone this repository
    
    
    cd ~
    git clone https://gitlab.com/rpiguru/PiPMD580

- Execute `setup.sh` to install everthing
    
    
    cd PiPMD580
    bash setup.sh

- And reboot!
