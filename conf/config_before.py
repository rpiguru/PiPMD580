import os

try:
    _is_rpi = 'arm' in os.uname()[4]
except AttributeError:
    _is_rpi = False

if _is_rpi:
    os.environ["KIVY_BCM_DISPMANX_LAYER"] = "4"
    os.environ["KIVY_BCM_DISPMANX_ID"] = "2"
    os.environ['KIVY_GL_BACKEND'] = 'gl'
