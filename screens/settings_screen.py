import os

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout

from screens.base import BaseScreen
from utils import logger
from utils.common import get_config, update_user_password, read_log, is_rpi
from widgets.dialog import PasswordDialog, YesNoDialog
from widgets.snackbar import show_info_snackbar

Builder.load_file('screens/settings_screen.kv')


class UserLabel(BoxLayout):
    user_level = NumericProperty()
    password = StringProperty()
    access_level = StringProperty()

    def on_touch_down(self, touch):
        super(UserLabel, self).on_touch_down(touch)
        if self.collide_point(*touch.pos):
            dlg = PasswordDialog(is_pwd=False, pwd=self.password)
            dlg.bind(on_success=self.on_pwd_changed)
            dlg.open()

    def on_pwd_changed(self, *args):
        args[0].dismiss()
        self.password = args[0].pwd
        if update_user_password(user_level=self.user_level, pwd=args[0].pwd):
            show_info_snackbar("Password changed")


class UsersWidget(BoxLayout):

    def __init__(self, **kwargs):
        super(UsersWidget, self).__init__(**kwargs)
        for user in get_config()['users']:
            self.ids.users_box.add_widget(UserLabel(**user))
        self.ids.users_box.add_widget(BoxLayout())


class DeviceWidget(BoxLayout):
    pass


class LogWidget(BoxLayout):

    def __init__(self, **kwargs):
        super(LogWidget, self).__init__(**kwargs)
        self.ids.lb_log.text = '\n'.join(read_log().splitlines()[-50:])
        Clock.schedule_once(lambda dt: self.scroll_bottom(), .3)

    def scroll_bottom(self):
        self.ids.lb_log.scroll_y = 0


class SettingsScreen(BaseScreen):

    def on_enter(self, *args):
        super(SettingsScreen, self).on_enter(*args)
        self.on_btn('users')

    def on_btn(self, _type='users'):
        for v in ['users', 'device', 'log']:
            if v == _type:
                self.ids['btn_{}'.format(v)].source = 'assets/images/btn_settings_selected.png'
            else:
                self.ids['btn_{}'.format(v)].source = 'assets/images/btn_settings.png'
        self.ids.box.clear_widgets()
        if _type == 'users':
            self.ids.box.add_widget(UsersWidget())
        elif _type == 'device':
            self.ids.box.add_widget(DeviceWidget())
        else:
            self.ids.box.add_widget(LogWidget())

    def on_btn_home(self):
        self.switch_screen('home_screen')

    def on_btn_reboot(self):
        dlg = YesNoDialog(message="Are you sure?")
        dlg.bind(on_confirm=self._on_confirm_reboot)
        dlg.open()

    @staticmethod
    def _on_confirm_reboot(*args):
        args[0].dismiss()
        logger.info('=== Rebooting Device...')
        if is_rpi():
            os.system("sudo reboot")

    def on_lock_state_changed(self):
        if self.app.locked:
            self.switch_screen('home_screen')
