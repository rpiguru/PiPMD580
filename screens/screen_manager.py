# -*- coding: iso8859-15 -*-
from kivy.uix.screenmanager import ScreenManager

from screens.home_screen import HomeScreen
from screens.settings_screen import SettingsScreen

screens = {
    'home_screen': HomeScreen,
    'settings_screen': SettingsScreen,
}


sm = ScreenManager()
