import datetime
import threading
import time

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty

from screens.base import BaseScreen
from utils import logger
from utils.common import add_log
# from widgets.snackbar import show_error_snackbar, show_info_snackbar


Builder.load_file('screens/home_screen.kv')


class HomeScreen(BaseScreen):

    state = ObjectProperty(None, allownone=True)

    def __init__(self, **kwargs):
        self._lock = threading.RLock()
        self.display_type = None
        super(HomeScreen, self).__init__(**kwargs)
        self._last_btn_time = 0
        self._rec_time = 0
        self.cnt = 0

    def on_enter(self, *args):
        super(HomeScreen, self).on_enter(*args)
        self.state = self.app.pmd.get_status()
        self._update_me()
        self.on_lock_state_changed()
        Clock.schedule_interval(lambda dt: self._show_datetime(), 1)
        self._update_display_type()
        Clock.schedule_interval(lambda dt: self._update_display_time(), 1)

    def _show_datetime(self):
        if self._b_stop.is_set():
            return False
        self.ids.lb_time.text = datetime.datetime.now().strftime("%H:%M:%S")
        self.ids.lb_date.text = datetime.datetime.now().strftime("%Y/%m/%d")
        if self.state == 'Recording':
            self._rec_time = min(self._rec_time + 1, 3600 * 1000 - 1)
            self.ids.lb_rec_time.text = 'Rec Time:  {0:03d}:{1:02d}:{2:02d}'.format(
                        self._rec_time // 3600, (self._rec_time % 3600) // 60, self._rec_time % 60)

    def on_lock_state_changed(self):
        if self.app.locked:
            if self.state == 'Recording':
                self.ids.btn.source = 'assets/images/btn_recording.png'
            else:
                pass
        else:
            if self.state == 'Recording':
                self.ids.btn.source = 'assets/images/btn_stop.png'
            else:
                self.ids.btn.source = 'assets/images/btn_record.png'
        self.ids.btn.opacity = 0 if (self.app.locked and self.state != 'Recording') else 1

    def on_btn(self):
        if time.time() - self._last_btn_time < 5:
            logger.warning('Button is pressed too frequently, ignoring...')
            return
        self._last_btn_time = time.time()
        if self.state == 'Recording':
            if self.app.pmd.stop_record():
                add_log('Stopped recording.')
                self.state = 'STOP'
                self.ids.btn.source = 'assets/images/btn_record.png'
                self._update_me()
                # show_info_snackbar('Stopped recording.')
            else:
                add_log('Failed to stop recording.')
                logger.error('Failed to stop recording..')
                # show_error_snackbar('Failed to stop recording.')
        else:
            if self.app.pmd.record():
                add_log('Started recording.')
                self.state = 'Recording'
                self.ids.btn.source = 'assets/images/btn_stop.png'
                self._rec_time = 0
                self._update_me()
                self.on_btn_display()
                # show_info_snackbar('Started recording.')
            else:
                add_log('Failed to start recording.')
                logger.error('Failed to start recording...')
                # show_error_snackbar('Failed to start recording.')

    def on_btn_display(self):
        if self.app.pmd.toggle_display_mode():
            # show_info_snackbar('Toggled Display Mode')
            self._update_display_type()
            self._update_display_time()
        else:
            logger.error('Failed to toggle time mode...')
            # show_error_snackbar('Failed to toggle time mode.')

    def on_touch_down(self, touch):
        super(HomeScreen, self).on_touch_down(touch)
        if not self.ids.img_lock.collide_point(*touch.pos) and not self.app.locked:
            if self.ids.box_status.collide_point(*touch.pos):
                logger.debug('Status bar is pressed, toggling display mode...')
                self.on_btn_display()

    def _update_me(self):
        file_name = self.app.pmd.get_track_name()
        logger.debug('Track Name: `{}`'.format(file_name))
        self.ids.lb_file_name.text = file_name if file_name else 'Not Available'

    def _update_display_type(self):
        display_type = self.app.pmd.get_time_mode()
        with self._lock:
            self.display_type = display_type
        logger.debug('Display Type: `{}`'.format(self.display_type))
        self.ids.lb_display_type.text = (self.display_type + ':') if self.display_type else ''

    def _update_display_time(self):
        if self._b_stop.is_set():
            return False
        t_str = self.app.pmd.get_display_time()
        if t_str:
            if self.display_type and 'Time' in self.display_type:
                # hhhmmss => hhh:mm:ss
                try:
                    t_str = '{}:{}:{}'.format(t_str[:3], t_str[3:5], t_str[-2:])
                except (IndexError, ValueError, AttributeError):
                    pass
        if self.cnt < 5:
            self.cnt += 1
        else:
            logger.debug('Display Time: `{}`'.format(t_str))
            self.cnt = 0
        self.ids.lb_display_value.text = t_str if t_str else ''

    def on_btn_settings(self):
        self.switch_screen('settings_screen')
