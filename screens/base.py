import threading
import time
from abc import abstractmethod

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen

from settings import SCREEN_OFF_LOGGED_IN, SCREEN_OFF_LOGGED_OUT
from utils import logger
from utils.common import add_log
from widgets.dialog import PasswordDialog, BlackPopup

Builder.load_file('screens/base.kv')


class BaseScreen(Screen):

    app = ObjectProperty()

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self._b_stop = threading.Event()
        self.app = App.get_running_app()
        self._last_active_time = time.time()
        self._black_popup = None
        Clock.schedule_interval(lambda dt: self._check_touch_input(), 1)

    def on_enter(self, *args):
        super(BaseScreen, self).on_enter(*args)
        self._b_stop.clear()

    def switch_screen(self, screen_name):
        self.app.switch_screen(screen_name=screen_name)

    def on_touch_down(self, touch):
        self._last_active_time = time.time()
        if self._black_popup is not None:
            return
        if self.ids.img_lock.collide_point(*touch.pos):
            if self.app.locked:
                dlg = PasswordDialog()
                dlg.bind(on_success=self.on_unlock_success)
                dlg.open()
            else:
                self.app.locked = True
                self.app.cur_user = {}
                self.on_lock_state_changed()
        else:
            if not self.app.locked:
                super(BaseScreen, self).on_touch_down(touch)

    def on_unlock_success(self, *args):
        args[0].dismiss()
        self.app.cur_user = args[1]
        add_log("User(level={}) logged in successfully.".format(self.app.cur_user.get('user_level')))
        self.app.locked = False
        self.on_lock_state_changed()

    @abstractmethod
    def on_lock_state_changed(self):
        pass

    def _check_touch_input(self):
        if self._b_stop.is_set():
            return False
        if self._black_popup is None:
            if self.name == 'home_screen' and getattr(self, 'state') == 'Recording':
                return
            offset = SCREEN_OFF_LOGGED_OUT if self.app.locked else SCREEN_OFF_LOGGED_IN
            if time.time() - self._last_active_time > offset:
                logger.info('=== Timeout: Logging out and turning display off...')
                if not self.app.locked:
                    self.app.locked = True
                    self.app.cur_user = {}
                self._black_popup = BlackPopup()
                self._black_popup.bind(on_dismiss=self.on_black_popup_closed)
                self._black_popup.open()
                if self.name != 'home_screen':
                    self.switch_screen('home_screen')

    def on_black_popup_closed(self, *args):
        self._black_popup = None
        self._last_active_time = time.time()

    def on_pre_leave(self, *args):
        self._b_stop.set()
        super(BaseScreen, self).on_pre_leave(*args)
