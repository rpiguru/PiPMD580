import os
import sys
import threading
import time
import serial.tools.list_ports
import serial

_par_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)

if _par_dir not in sys.path:
    sys.path.append(_par_dir)

from utils.common import is_rpi
from utils import logger

RESPONSES = {
    'state': {
        '@0STRU': 'Din Unlock',
        '@0STRE': 'Recording',
        '@0STRP': 'RecPause',
        '@0STST': 'STOP',
        '@0STTS': 'TRACK Select',
        '@0STSH': 'Scheduled',
        '@0STPL': 'PLAY',
        '@0STPP': 'PLAYPAUSE',
        '@0STS+': 'SEEK +',
        '@0STS-': 'SEEK -',
        '@0STFF': 'FF',
        '@0STRW': 'RWD',
        '@0STED': 'TrackEdit/Preset',
        '@0STER': 'Operation Error'
    },
    'time_mode': {
        '@0TMRE': 'RecRemainTime',
        '@0TMAG': 'Algo',
        '@0TMTT': 'TrackTime',          # Elapsed Time
        '@0TMCD': 'CurrentDate',
        '@0TMCT': 'CurrentTime',
        '@0TMRT': 'RecordedTime',
        '@0TMRD': 'RecordedDate',
        '@0TMFN': 'FileName',
        '@0TMTR': 'TrackRemainTime',
        '@0TMCH': 'RecordedChannel'
    },
    'card': {
        "@0CDNC": "No Card",
        "@0CDRE": "Reading",
        "@0CDCI": "Card In",
        "@0CDCE": "Card Error",
        "@0CD99": "Card 999",
        "@0CDFL": "Card Full",
        "@0CDBC": "Blank Card",
    },
    'power': {
        "@0PW00": "ON",
        "@0PW01": "Standby"
    },
    "door": {
        "@0DS01": "Open",
        "@0DS00": "Close"
    }
}


ACK_RESP = "\x06"


class PMD580(threading.Thread):

    _ser = None
    _lock = threading.RLock()
    _b_stop = threading.Event()

    def __init__(self, vid=0x067B, pid=0x2303, baudrate=9600, timeout=.2, callback=None):
        super().__init__()
        self.vid = vid
        self.pid = pid
        self.baudrate = baudrate
        self.timeout = timeout
        self.callback = callback
        self.open()

    def open(self):
        port = None
        for dev in serial.tools.list_ports.comports():
            if dev.pid == self.pid and dev.vid == self.vid:
                port = dev.device
                logger.info('Found the PLM dongle - {}'.format(port))
        if port is not None:
            try:
                self._ser = serial.Serial(port=port, baudrate=self.baudrate, timeout=self.timeout)
                return True
            except Exception as e:
                logger.error('PLM: Failed to open device({}) - {}'.format(port, e))

    def run(self):
        self._b_stop.clear()
        while not self._b_stop.is_set():
            sleep_time = .2
            buf = ''
            with self._lock:
                if is_rpi():
                    if self._ser is None and not self.open():
                        sleep_time = 1
                    else:
                        try:
                            buf = self._ser.readline().decode().strip()
                        except Exception as e:
                            logger.error('Failed to read data from PMD580 - {}'.format(e))
                            self._ser = None
                else:
                    time.sleep(self.timeout)
            if buf and callable(self.callback):
                logger.info('PMD580: Read data - `{}`'.format(buf))
                self._parse_msg(buf.splitlines()[-1])

            time.sleep(sleep_time)

    def _send_command(self, cmd, is_ctrl=False):
        """
        Send a command to PMD580
        :param cmd:
        :type cmd: str
        :return:
        """
        logger.debug('Sending command to PMD580 - `{}`'.format(cmd))
        if not is_rpi():
            return True
        with self._lock:
            if self._ser is None:
                if not self.open():
                    return
            if not cmd.endswith('\r'):
                cmd += '\r'
            self._ser.write(cmd.encode())
            time.sleep(.2)
            buf = self._ser.readline().decode().strip()
        if is_ctrl:
            return ACK_RESP in buf
        else:
            return buf

    def _parse_msg(self, buf):
        for k in RESPONSES:
            if buf in RESPONSES[k]:
                self.callback({'type': k, 'value': RESPONSES[k][buf]})
                return
        self.callback({'type': None, 'value': buf})

    # ======================================== Control Commands ==================================================
    def stand_by(self):
        return self._send_command('@02312', is_ctrl=True)

    def reboot(self):
        return self._send_command('@0RBT', is_ctrl=True)

    def power_on(self):
        return self._send_command('@020PW', is_ctrl=True)

    def record(self):
        return self._send_command('@02355', is_ctrl=True)

    def start_playback(self):
        return self._send_command('@02353', is_ctrl=True)

    def pause_playback(self):
        return self._send_command('@02348', is_ctrl=True)

    def stop_record(self):
        return self._send_command('@02354', is_ctrl=True)

    def toggle_display_mode(self):
        return self._send_command('@02311', is_ctrl=True)

    # ======================================== Status Commands ==================================================

    @staticmethod
    def __parse_response(resp, keyword):
        resp = str(resp)
        if resp is not None and keyword in resp:
            return resp.split(keyword)[1]

    def get_power_mode(self):
        resp = self._send_command('@0?PW')
        if resp == '@0PW00':
            return 'On'
        elif resp == '@0PW01':
            return 'Standby'

    def get_status(self):
        return RESPONSES['state'].get(self._send_command('@0?ST'))

    def get_card_status(self):
        return RESPONSES['card'].get(self._send_command('@0?CD'))

    def get_track_number(self):
        resp = self._send_command('@0?TR')
        num = self.__parse_response(resp=resp, keyword='@0TR')
        try:
            return int(num)
        except (AttributeError, ValueError, TypeError):
            pass

    def get_time_mode(self):
        return RESPONSES['time_mode'].get(self._send_command('@0?TM'))

    def get_display_time(self):
        resp = self._send_command('@0?TI')
        return self.__parse_response(resp=resp, keyword='@0TI')

    def get_playback_volume(self):
        resp = self._send_command('@0?VM')
        vol = self.__parse_response(resp=resp, keyword="@0VM")
        try:
            return int(vol)
        except (AttributeError, ValueError, TypeError):
            pass

    def get_record_volume(self):
        resp = self._send_command('@0?RV')
        return self.__parse_response(resp=resp, keyword="@0RV")

    def get_total_track_number(self):
        resp = self._send_command('@0?TT')
        num = self.__parse_response(resp=resp, keyword="@0TT")
        try:
            return int(num)
        except (AttributeError, ValueError):
            pass

    def get_track_name(self):
        track_num = self.get_track_number()
        if track_num is not None:
            resp = self._send_command('@0?Tn{0:03d}'.format(track_num))
            return self.__parse_response(resp=resp, keyword='@0Tn')

    def get_track_size(self):
        track_num = self.get_track_number()
        if track_num is not None:
            resp = self._send_command('@0?Ts{0:03d}'.format(track_num))
            return self.__parse_response(resp=resp, keyword='@0Ts')

    def get_folder_name_list(self):
        resp = self._send_command('@0?FL')
        fl = self.__parse_response(resp=resp, keyword="@0FL")
        if fl:
            return fl.split(';')
        else:
            return []

    def get_machine_name(self):
        resp = self._send_command('@0?MN')
        return self.__parse_response(resp=resp, keyword="@0MN")

    def get_version(self):
        resp = self._send_command('@0?VN')
        return self.__parse_response(resp=resp, keyword="@0VN")

    def close(self):
        self._b_stop.set()
        if self._ser:
            try:
                self._ser.close()
            except Exception as e:
                logger.error('PLM: Failed to close - {}'.format(e))
        self._ser = None


if __name__ == '__main__':

    def on_new_status(msg):
        print('New status: `{}`'.format(msg))

    pmd = PMD580(callback=on_new_status)
    print('Power mode: {}'.format(pmd.get_power_mode()))
    pmd.start()
