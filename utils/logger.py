import datetime
import os

from kivy.logger import Logger


PREFIX = 'PMD'


def get_local_git_commit_date():
    pipe = os.popen('git log -1 --format=%cd --date=local')
    data = pipe.read().strip()
    pipe.close()
    return data


commit_date = get_local_git_commit_date()


def debug(msg):
    Logger.debug('{}({}) - {}: {}'.format(PREFIX, commit_date, datetime.datetime.now(), msg))


def info(msg):
    Logger.info('{}({}) - {}: {}'.format(PREFIX, commit_date, datetime.datetime.now(), msg))


def warning(msg):
    Logger.warning('{}({}) - {}: {}'.format(PREFIX, commit_date, datetime.datetime.now(), msg))


def error(msg):
    Logger.error('{}({}) - {}: {}'.format(PREFIX, commit_date, datetime.datetime.now(), msg))


def exception(e):
    Logger.exception('{}({}) - {}: {}'.format(PREFIX, commit_date, datetime.datetime.now(), e))
